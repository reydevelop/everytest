var ProductCore = function ()
{
//    properties
    var processorUrl = null;
    var self = this;
    this.products = null;
    var counterInstanceModule = null;
    var productInstanceModule = null;
    this.productsArrayInstanceModule = null;
    var index = null;

//    general function
    this.addListener = function (htmlObject_p, eventType_p, fnCallback_p, myObject_p)
    {
        return $(htmlObject_p).bind(eventType_p, myObject_p, fnCallback_p);
    };

    this.removeListener = function (htmlObject_p, eventType_p, fnCallback_p)
    {
        $(htmlObject_p).unbind(eventType_p, fnCallback_p);
    };

    this.get = function (element_p)
    {
        return $('#' + element_p).get(0);
    };

    this.getAttrById = function (elementId, attribute)
    {
        if (typeof elementId == "string") {
            return $("#" + elementId).attr(attribute);
        }
        return $(elementId).attr(attribute);
    };

    this.getValue = function (element) {
        return $(element).val();
    };

    this.clearValue = function (elementId) {
        if (typeof elementId == "string") {
            $("#" + elementId).val('');
        }
        else
        {
            $(elementId).val('');
        }
    };

    this.onlyNumbers = function (e, obj)
    {
        var key = (document.all) ? e.keyCode : e.which;
        if (key == 8 || key == 0 || key == 26 || key == 127 || (key >= 48 && key <= 57))
        {
            return true;
        }
        else
        {
            e.preventDefault();
        }
    };

    var removeItemFromArray = function (arr, key) {
        if (!arr.hasOwnProperty(key))
            return
        if (isNaN(parseInt(key)) || !(arr instanceof Array))
            delete arr[key];
        else
            delete self.productsArrayInstanceModule[key];
    };

//    create a module
    var createAddProductModule = function (moduleId_p, data_p, sandbox_p)
    {
        var instance = null;
        instance = new AddProductModule();
        instance.init(moduleId_p, data_p, sandbox_p);
        return instance;
    };

    var startAddProductModule = function ()
    {
        var data_p = null;
        var sandbox = new ProductSandbox();
        sandbox.init(self);
        createAddProductModule('addProductModuleId', data_p, sandbox);
    };

//create counter module
    var createCounterProductModule = function (moduleId_p, data_p, sandbox_p)
    {
        var instance = null;
        instance = new CounterProductModule();
        instance.init(moduleId_p, data_p, sandbox_p);
        return instance;
    };
    var startCounterProductModule = function ()
    {
        var data_p = null;
        var sandbox = new ProductSandbox();
        sandbox.init(self);
        counterInstanceModule = createCounterProductModule('counterProductModuleId', data_p, sandbox);

    };

//create product module
    var createProductModule = function (moduleId_p, data_p, sandbox_p)
    {
        var instance = null;
        instance = new ProductModule();
        instance.init(moduleId_p, data_p, sandbox_p);
        return instance;
    };
    var startProductModule = function (data_p)
    {
//        var data_p = {"index": self.index, "name": name, "price": price, "amount": amount};
        //setear el index para el borrado del DOM
        var sandbox = new ProductSandbox();
        sandbox.init(self);
        productInstanceModule = createProductModule("productModule" + self.index, data_p, sandbox);
        self.productsArrayInstanceModule["productModule" + self.index.toString()] = productInstanceModule;
        return productInstanceModule;
    };

//    operation functions
    this.getTotalAmount = function () {
        var current_amount = 0;
        for (var key in self.productsArrayInstanceModule) {
            if (self.productsArrayInstanceModule.hasOwnProperty(key))
//                console.log(self.productsArrayInstanceModule[key].getAmount());
                current_amount = current_amount + parseInt(self.productsArrayInstanceModule[key].getAmount());
        }
        return current_amount;
    };

//    HTML writers

    this.appendHtml = function (Object_html, html) {
        $(Object_html).append(html);
    };

    this.setHtml = function (htmlObject_p, current_amount) {
        $(htmlObject_p).html(current_amount);
    };

//    product handler


    this.removeElement = function (elementId) {
        if (typeof elementId == "string") {
            return $("#" + elementId).remove();
        }
        return $(elementId).remove();

    };

    //get element to delete
//    this.getProduct = function (position) {
//        return self.productsArrayInstanceModule[position];
//    };

    this.handleNotification = function (event_p, data_p)
    {
        data_p["index"] = self.index;
        switch (event_p)
        {
            case 'get-txt-values':

//                var trId = "tr-" + self.index;


//                var html = product.getHtml(trId);
//                console.log(html);
//                var html = "<tr id = " + trId + "><td>" + data_p.name + "</td><td>" + data_p.price + "</td><td><input type='text' class='form-control' id='update_amount' name='update_amount' value=" + data_p.amount + "></td><td><input type='button' id='btn_delete" + self.index + "' name='btn_delete" + self.index + "' value='Delete'></td></tr>";
//                self.appendHtml('#product_list', html);
                var product = startProductModule(data_p);
                console.log(data_p);

//                console.log(data_p.amount);
//                var product_html = product.getHtml(trId);
//                console.log(product_html);
                self.index++;
//                una vez que se adiciona se debe actualizar el contador
                var cantidad = self.getTotalAmount();
                counterInstanceModule.setLbCounterHtml(cantidad);
                console.log(self.productsArrayInstanceModule);
                break;

            case 'remove-product':
                console.log(data_p);
                var id = "productModule" + data_p;
                removeItemFromArray(self.productsArrayInstanceModule, id);
                var cantidad = self.getTotalAmount();
                counterInstanceModule.setLbCounterHtml(cantidad);
//                counterInstanceModule.updateCounter();
                console.log(self.productsArrayInstanceModule);
                break;
            default:
                break;
        }
    };


//    this.setCounter = function (htmlObject_p) {
//        var current_amount = 0;
//        for (var key in self.productsArrayInstanceModule) {
//            if (self.productsArrayInstanceModule.hasOwnProperty(key))
//                console.log(self.productsArrayInstanceModule[key].getAmount());
//            current_amount = current_amount + parseInt(self.productsArrayInstanceModule[key].amount);
//        }
//        $(htmlObject_p).html(current_amount);
//    };






    this.init = function ()
    {
        //Set initial values for variables
        var procesorUrl = "";
        self.products = [];
        self.index = 0;
        self.productsArrayInstanceModule = [];
        //fill associative array
        startAddProductModule();

        startCounterProductModule();
        var cantidad = self.getTotalAmount();
        counterInstanceModule.setLbCounterHtml(cantidad);
    };

};


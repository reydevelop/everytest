var ProductSandbox = function ()
{
    var core = null;
    this.init = function (core_p)
    {
        core = core_p;
    };
    /*
     Function: addListener
     Parameters: htmlObject_p : An id, an element reference, or a collection of ids and/or elements to assign the listener to
     eventType_p:The type of event to append
     fnCallback_p:The method the event invokes
     myObject_p:An arbitrary object that will be passed as a parameter to the handler
     scopeObject_p:   If true, the obj passed in becomes the execution context of the listener. If an object, this object becomes the execution context.
     Returns: True if the action was successful or defered, false if one or more of the elements could not have the listener attached, or if the operation throws an exception.
     */
    this.addListener = function (htmlObject_p, eventType_p, fnCallback_p, myObject_p, scopeObject_p)
    {
        //returns true if the event handler was attached succesfully
        return core.addListener(htmlObject_p, eventType_p, fnCallback_p, myObject_p, scopeObject_p);
    };
//    this.setCounter = function (htmlObject_p) {
//        core.setCounter(htmlObject_p);
//    };
    /*
     Function: removeListener => static boolean removeListener ( el , sType , fn ) => Removes an event listener
     Parameters:     el <String|HTMLElement|Array|NodeList> An id, an element reference, or a collection of ids and/or elements to remove the listener from. 
     sType <String> the type of event to remove. 
     fn <Function> the method the event invokes. If fn is undefined, then all event handlers for the type of event are removed. 
     Returns: boolean
     true if the unbind was successful, false otherwise.
     
     */
    this.removeListener = function (element_p, eventType_p, fnCallback_p)
    {
        return core.removeListener(element_p, eventType_p, fnCallback_p);
    };

    /*
     Function: notify
     Parameters: event_p : case of event, data_p: a json object
     Returns: Notify whose event has to do.
     */
    this.notify = function (event_p, data_p)
    {
        core.handleNotification(event_p, data_p);
    };

    /*
     Function selectorQuery
     Parameters: selector (<string> The CSS Selector to test the node against),
     root (<HTMLElement|String> optional An id or HTMLElement to start the query from. Defaults to Selector.document.),
     firstOnly (<Boolean> optional Whether or not to return only the first match.)
     Returns:An array of nodes that match the given selector.
     */
    this.selectorQuery = function (selector_p, root_p, firstOnly_p)
    {
        return core.selectorQuery(selector_p, root_p, firstOnly_p);
    };
    /*
     Function stopEvent
     Parameters: ev (<Event> the event)
     Convenience method for stopPropagation + preventDefault
     */

    this.onlyLetters = function (ev_p, obj_p)
    {
        return core.onlyLetters(ev_p, obj_p);
    };

    this.onlyNumbers = function (ev_p, obj_p)
    {
        return core.onlyNumbers(ev_p, obj_p);
    };
    this.hasClass = function (element_p, className_p)
    {
        return core.hasClass(element_p, className_p);
    };
    this.addClass = function (element_p, className_p)
    {
        return core.addClass(element_p, className_p);
    };
    this.removeClass = function (element_p, className_p)
    {
        return core.removeClass(element_p, className_p);
    };
    this.replaceClass = function (element_p, oldClassName_p, newClassName_p)
    {
        return core.replaceClass(element_p, oldClassName_p, newClassName_p);
    };
    // get the value from form
    this.get = function (element_p)
    {
        return core.get(element_p);
    };

    this.setHtml = function (element_p, html){
      core.setHtml(element_p, html);  
    };
    this.appendHtml = function (element_p, html){
      core.appendHtml(element_p, html);  
    };
    this.getElementsByClassName = function (className_p, tag_p, root_p, apply_p, o_p, overrides_p)
    {
        return core.getElementsByClassName(className_p, tag_p, root_p, apply_p, o_p, overrides_p);
    };
    this.delayFunction = function (data_p)
    {
        return core.delayFunction(data_p);
    };
    this.getValue = function (element) {
        return core.getValue(element);
    };
    this.clearValue = function(element){
        core.clearValue(element);
    };
    /*Function: trim
     *removes rigth and left blank spaces
     *Parameters: string
     *Return: string without blank spaces.
     **/
    this.trim = function (data)
    {
        return core.trim(data);
    };
    //Christian 04-07-2014 use jquery to get attributes
    this.getAttrById = function (elementId, attribute)
    {
        return core.getAttrById(elementId, attribute);
    };
    this.removeElement = function (elementId) {
        core.removeElement(elementId);
    };
};
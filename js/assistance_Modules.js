/**
 * 
 * @returns {CheckAssistanceModule}
 * this module will check if employee exist an set the time on each event
 */

var CheckAssistanceModule = function ()
{
    var processorUrl = "/_testrey/everytest/php/assistance_MDL.php";
    var moduleId = null;
    var sandbox = null;
    var txt_ci = null;
    var btn_chk = null;
    var data = null;

//    success function definition
    var saveCheck_success = function (response) {
        response = JSON.parse(response);
        console.log(response.status);
        if (response.status != 'failure') {
            sandbox.notify("chk-ci", response);
        }
        else {
            sandbox.notify("popup-alert", response);
//            console.log(response);
        }
    };
//    error function definition
    var saveCheck_failure = function (jqXHR, textStatus, errorThrown)
    {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    };
//    asynchrounous function definition
    var saveCheck = function (txt_ci_number)
    {
        // create the AJAX options object and then call sandbox function to initiate the AJAX request
        var requestObj = {"ci": txt_ci_number};
        var ajaxObj = {
            url: processorUrl,
            type: "get",
//            dataType: "json",
            data: requestObj,
            success: saveCheck_success,
            error: function (jqXHR, textStatus, error) {
                saveCheck_failure(jqXHR, textStatus, error);
            }
        };

        sandbox.asyncRequest(ajaxObj);
    };

    var btnChk_click = function (ev) {
        var txt_ci_val = sandbox.getValue(txt_ci);
        sandbox.clearValue(txt_ci);
        var arr = {"ci": txt_ci_val};
        saveCheck(txt_ci_val);
    };

    var onlyNumbers = function (ev)
    {
        sandbox.onlyNumbers(ev);
    };

    this.init = function (moduleId_p, data_p, sandbox_p)
    {
        console.log('init CheckAssistance module');
        //Get parameters
        moduleId = moduleId_p;
        sandbox = sandbox_p;
        data = data_p;
        sandbox.dummyConsole('probando herencia...');
        //Get post value
        txt_ci = sandbox.get("txt_ci");
        sandbox.addListener(txt_ci, "keypress", onlyNumbers, self, true);

//        sandbox.addListener(txt_ci, "blur", btnChk_click, null, null);

        btn_chk = sandbox.get("btn_chk");
        sandbox.addListener(btn_chk, "click", btnChk_click, null, null);

    };
};
/**
 * 
 * @returns {ShowAssistanceModule}
 * this module will show the employee record on the day everytime he/she check assistance
 */
var ShowAssistanceModule = function () {
    var moduleId = null;
    var sandbox = null;
    var data = null;
//html properties
    var label_name = null;
    var label_chk_in = null;
    var label_lunch_out = null;
    var label_lunch_in = null;
    var label_chk_out = null;
    var employee_control = null;


//    functions

    this.setDataHtml = function (data_p) {
        data = data_p;
        sandbox.setHtml(employee_control, data.html);
    };

    this.init = function (moduleId_p, data_p, sandbox_p)
    {
        console.log('init ShowAssistance module');
        //Get parameters
        moduleId = moduleId_p;
        sandbox = sandbox_p;
        data = data_p;

//        Retrieving html properties
        label_name = sandbox.get("employee_name");
        label_chk_in = sandbox.get("chk_in");
        label_lunch_out = sandbox.get("lunch_out");
        label_lunch_in = sandbox.get("lunch_in");
        label_chk_out = sandbox.get("chk_out");
        employee_control = sandbox.get("employee_control");
    };
};

var ShowModalModule = function () {
    var moduleId = null;
    var sandbox = null;
    var data = null;
    //html properties
    this.modal_box = null;
    this.label_message = null;
    var label_suggestion = null;


    //    functions
//    this.setDataHtml = function (data_p) {
//        data = JSON.parse(data_p);
//        sandbox.setHtml(label_message, data.message);
//        sandbox.setHtml(label_suggestion, data.suggestion);
//    };
    this.showModal = function (data) {
        console.log(data.message);
        sandbox.setHtml(label_message, data.message);
        sandbox.setHtml(label_suggestion, data.suggestion);
        sandbox.showModalCore(modal_box);
    };
    this.getModalBox=function(){
        return modal_box;
    };

    this.init = function (moduleId_p, data_p, sandbox_p)
    {
        console.log('init ShowModal module');
        //Get parameters
        moduleId = moduleId_p;
        sandbox = sandbox_p;
        data = data_p;

//        Retrieving html properties
        modal_box = sandbox.get("popup-notification");
        label_message = sandbox.get("label_message");
        label_suggestion = sandbox.get("label_suggestion");

    };
};
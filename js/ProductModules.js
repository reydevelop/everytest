/*************************************Start BillingInfoModule********************/
var AddProductModule = function ()
{
    var processorUrl = null;
    var moduleId = null;
    var sandbox = null;
    var txt_product_name = null;
    var txt_product_price = null;
    var txt_product_amount = null;
    var btn_add = null;
    var data = null;


    var btnAdd_click = function (ev) {
        var txt_product_name_val = sandbox.getValue(txt_product_name);
        sandbox.clearValue(txt_product_name);
        var txt_product_price_val = sandbox.getValue(txt_product_price);
        sandbox.clearValue(txt_product_price);

        var txt_product_amount_val = sandbox.getValue(txt_product_amount);


        sandbox.clearValue(txt_product_amount);

        var arr = {"name": txt_product_name_val, "price": txt_product_price_val, "amount": txt_product_amount_val};
        sandbox.notify("get-txt-values", arr);
    };
    var onlyNumbers = function (ev)
    {
        sandbox.onlyNumbers(ev);
    };



    this.init = function (moduleId_p, data_p, sandbox_p)
    {
        console.log('init ADD module');
        //Get parameters
        moduleId = moduleId_p;
        sandbox = sandbox_p;
        var data = data_p;

        //Get post value
        txt_product_name = sandbox.get("txt_name");
        txt_product_price = sandbox.get("txt_price");
        sandbox.addListener(txt_product_price, "keypress", onlyNumbers, self, true);
        txt_product_amount = sandbox.get("txt_amount");
        sandbox.addListener(txt_product_amount, "keypress", onlyNumbers, self, true);

        btn_add = sandbox.get("btn_add");
        sandbox.addListener(btn_add, "click", btnAdd_click, null, null);

    };
};

var CounterProductModule = function () {
    var processorUrl = null;
    var moduleId = null;
    var sandbox = null;
    var lb_counter = null;
    var self = this;

    this.updateCounter = function () {
        sandbox.setCounter(lb_counter);
    };

    this.getLbCounter = function () {
        return lb_counter;
    };
    this.setLbCounterHtml = function (cantidad) {
        sandbox.setHtml(lb_counter, cantidad);
    };
    this.init = function (moduleId_p, data_p, sandbox_p)
    {
        console.log('init counter module');
        moduleId = moduleId_p;
        sandbox = sandbox_p;
        var data = data_p;
        lb_counter = sandbox.get("lb_counter");
        //set value
//        sandbox.setCounter(lb_counter);
    };
};
var ProductModule = function () {
    var processorUrl = null;
    var moduleId = null;
    var sandbox = null;
    var link = null;
    var btn_delete = null;
    var txt_amount = null;
    var data = null;
    var price = null;
    var name = null;
    var amount = null;
    var index = null;
    var productsContainer = null;

    var btnDelete_click = function () {
        console.log(data);
        sandbox.removeElement("tr-" + data['index']);
        sandbox.notify("remove-product", data['index']);
    };

    this.getId = function () {
        return moduleId;
    };
    this.getName = function () {
        return name;
    };
    this.getPrice = function () {
        return price;
    };
    this.getAmount = function () {
        return amount;
    };
    var setAmount = function (newval) {

        console.log("newval");
        amount = newval;
    };
    this.getHtml = function (trId) {
        var html = "<tr id = " + trId + "><td>" + this.name + "</td><td>" + this.price + "</td><td><input type='text' class='form-control update-amount' id='update_amount" + index_p + "' name='update_amount' value=" + this.amount + "></td><td><input type='button' id='btn_delete" + this.index + "' name='btn_delete" + this.index + "' value='Delete'></td></tr>";
        return html;
    };
    this.getProductContainer = function () {

    };
    var insertNewProduct = function (index_p, name_p, price_p, amount_p) {
        var html = "<tr id = 'tr-" + index_p + "'><td>" + name_p + "</td><td>" + price_p + "</td><td><input type='text' class='form-control  update-amount' id='update_amount" + index_p + "' name='update_amount' value=" + amount_p + "></td><td><input type='button' id='btn_delete" + index_p + "' name='btn_delete" + index_p + "' value='Delete'></td></tr>";
        sandbox.appendHtml(productsContainer, html);
    };


    var txtAmount_change = function () {
        var txt_amount_up = sandbox.getValue(txt_amount);
//        console.log(data);
        data.amount = txt_amount_up;
//        console.log(data);
        sandbox.notify("change-amount", data);

    };
    this.init = function (moduleId_p, data_p, sandbox_p)
    {
        console.log('init product module');
//        console.log(data_p);
        moduleId = moduleId_p;
        name = data_p.name;
        price = data_p.price;
        amount = data_p.amount;
        index = data_p.index;

        sandbox = sandbox_p;
        data = data_p;
        productsContainer = sandbox.get("product_list");

        insertNewProduct(index, name, price, amount);

        btn_delete = sandbox.get("btn_delete" + index);
        txt_amount = sandbox.get("update_amount" + index);
//        console.log(productsContainer);
        sandbox.addListener(btn_delete, "click", btnDelete_click, null, null);
//        sandbox.addListener(txt_amount, "blur", txtAmount_change, null, null);

    };
};
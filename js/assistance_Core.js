var AssistanceCore = function ()
{
//    properties
    var processorUrl = null;
    var self = this;
    var checkInstanceModule = null;
    var showInstanceModule = null;
    var modalInstanceModule = null;

//    general function
    this.addListener = function (htmlObject_p, eventType_p, fnCallback_p, myObject_p)
    {
        return $(htmlObject_p).bind(eventType_p, myObject_p, fnCallback_p);
    };

    this.removeListener = function (htmlObject_p, eventType_p, fnCallback_p)
    {
        $(htmlObject_p).unbind(eventType_p, fnCallback_p);
    };

    this.get = function (element_p)
    {
        return $('#' + element_p).get(0);
    };

    this.getAttrById = function (elementId, attribute)
    {
        if (typeof elementId == "string") {
            return $("#" + elementId).attr(attribute);
        }
        return $(elementId).attr(attribute);
    };

    this.getValue = function (element) {
        return $(element).val();
    };

    this.clearValue = function (elementId) {
        if (typeof elementId == "string") {
            $("#" + elementId).val('');
        }
        else
        {
            $(elementId).val('');
        }
    };

    this.onlyNumbers = function (e, obj)
    {
        var key = (document.all) ? e.keyCode : e.which;
        if (key == 8 || key == 0 || key == 26 || key == 127 || (key >= 48 && key <= 57))
        {
            return true;
        }
        else
        {
            e.preventDefault();
        }
    };

/**
 * MODULES
 */
//    create a CheckAssistanceModule module
    var createCheckAssistanceModule = function (moduleId_p, data_p, sandbox_p)
    {
        var instance = null;
        instance = new CheckAssistanceModule();
        instance.init(moduleId_p, data_p, sandbox_p);
        return instance;
    };

    var startCheckAssistanceModule = function ()
    {
        var data_p = null;
        var sandbox = new AssistanceSandbox();
        
        sandbox.init(self);
        sandbox.initBase(self);
        checkInstanceModule = createCheckAssistanceModule('CheckAssistanceModuleId', data_p, sandbox);
    };
    //    create a ShowAssistanceModule module
    var createShowAssistanceModule = function (moduleId_p, data_p, sandbox_p)
    {
        var instance = null;
        instance = new ShowAssistanceModule();
        instance.init(moduleId_p, data_p, sandbox_p);
        return instance;
    };
        var startShowAssistanceModule = function ()
    {
        var data_p = null;
        var sandbox = new AssistanceSandbox();
        sandbox.init(self);
        //sandbox.initBase(self);
        
        showInstanceModule = createShowAssistanceModule('ShowAssistanceModuleId', data_p, sandbox);
    };
    //    create a ShowModalModule module
    var createShowModalModule = function (moduleId_p, data_p, sandbox_p)
    {
        var instance = null;
        instance = new ShowModalModule();
        instance.init(moduleId_p, data_p, sandbox_p);
        return instance;
    };
    var startShowModalModule = function ()
    {
        var data_p = null;
        var sandbox = new AssistanceSandbox();
        
        sandbox.init(self);
        //sandbox.initBase(null);
        modalInstanceModule = createShowModalModule('ShowModalModuleId', data_p, sandbox);
    };

    this.dummyConsole =function(param){
        console.log(param);
    };
//    HTML writers
    this.appendHtml = function (Object_html, html) {
        $(Object_html).append(html);
    };

    this.setHtml = function (htmlObject_p, data_p) {
        $(htmlObject_p).html(data_p);
    };
    this.showModal=function(htmlObject_p, data_p){        
//        show the modal
        $(htmlObject_p).modal();
    };

//     handler
    this.removeElement = function (elementId) {
        if (typeof elementId == "string") {
            return $("#" + elementId).remove();
        }
        return $(elementId).remove();

    };

    this.asyncRequest = function (dataObj)
    {
        $.ajax(dataObj);
    };

    this.handleNotification = function (event_p, data_p)
    {
       
        switch (event_p)
        {
            case 'chk-ci':
                console.log('se llamo al boton');
//                set data to show module instance
                showInstanceModule.setDataHtml(data_p);
                break;
            case 'popup-alert':
                console.log('se mostro el popup');
                console.log(data_p);

                modalInstanceModule.showModal(data_p);   
                showInstanceModule.setDataHtml(data_p);
            break;
            default:
                break;
        }
    };
//    functions
    this.showModalCore = function(htmlObject_p){       
        $(htmlObject_p).modal();
    };

    this.init = function ()
    {
        //Set initial values for variables
        var procesorUrl = "";

        startCheckAssistanceModule();
//        console.log(checkInstanceModule);
        startShowAssistanceModule();
        startShowModalModule();
//        console.log(modalInstanceModule);
    };

};


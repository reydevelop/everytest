<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/_testrey/everytest/php/AssistanceModel.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/_testrey/everytest/php/AssistanceView.php");

class AssistanceCore {

    // properties
    private $assistanceModel = null;
    private $assistanceView = null;

    public function __construct() {
        $this->assistanceModel = new AssistanceModel();
        $this->assistanceView = new AssistanceView();
    }

//        if the employee exists return true
    public function checkEmployeeExist($ci) {
        $employee = $this->assistanceModel->getEmployee($ci);
        if ($employee) {
            return $employee[0];
        } else {
            return false;
        }
    }

//    check if the has filled all their in/out 
    public function checkEmployeeRecord($ci) {
        $recordSet = $this->assistanceModel->getEmployeeDayRecord($ci);
        return $recordSet;
    }

//    if the record is complete return true
//    otherwise return the name of field to fill
    public function checkFullEmployeeRecord($recordSet) {
        if (empty($recordSet)) {
            return false;
        }

        $fieldToFill = '';
        if ($recordSet['chk_in'] == '00:00:00' || $recordSet['chk_in'] == null) {
            $fieldToFill = 'chk_in';
            return $fieldToFill;
        } else if ($recordSet['lunch_out'] == '00:00:00' || $recordSet['lunch_out'] == null) {
            $fieldToFill = 'lunch_out';
            return $fieldToFill;
        } else if ($recordSet['lunch_in'] == '00:00:00' || $recordSet['lunch_in'] == null) {
            $fieldToFill = 'lunch_in';
            return $fieldToFill;
        } else if ($recordSet['chk_out'] == '00:00:00' || $recordSet['chk_out'] == null) {
            $fieldToFill = 'chk_out';
            return $fieldToFill;
        }
        return true;
    }

    public function updateEmployeeRecord($employeeId, $fieldToUpdate) {
        $this->assistanceModel->updateEmployeeDayRecord($employeeId, $fieldToUpdate);
    }

    public function addEmployeeRecord($employeeId) {
        $this->assistanceModel->insertEmployeeDayRecord($employeeId);
    }

    public function getAssistanceHtml($AssistanceInfoArray) {
        $html = $this->assistanceView->AssistanceView($AssistanceInfoArray);
        return $html;
    }

    public function getHtml($ci) {
        $record = $this->checkEmployeeRecord($ci);
        $infoArray = array(
            'name' => $record['name'] . ' ' . $record['lastname'],
            'chk_in' => $record['chk_in'],
            'lunch_out' => $record['lunch_out'],
            'lunch_in' => $record['lunch_in'],
            'chk_out' => $record['chk_out']);
        $employeeAssistanceHtml = $this->getAssistanceHtml($infoArray);
        return $employeeAssistanceHtml;
    }

    public function getResponse($ci) {
        $recordSet = $this->checkEmployeeRecord($ci);
//    check if the record is full
        $chk = $this->checkFullEmployeeRecord($recordSet);
        if ($chk === true) {
            // if the record it is full return a fault json reporting it
            // to improve the example I ll return the data employee info too

//            $record = $this->checkEmployeeRecord($ci);
//            $infoArray = array(
//                'name' => $record['name'] . ' ' . $record['lastname'],
//                'chk_in' => $record['chk_in'],
//                'lunch_out' => $record['lunch_out'],
//                'lunch_in' => $record['lunch_in'],
//                'chk_out' => $record['chk_out']);
//
//            $employeeAssistanceHtml = $this->getAssistanceHtml($infoArray);
            $employeeAssistanceHtml = $this->getHtml($ci);
//            var_dump($employeeAssistanceHtml);
            $respuesta = array(
                'status' => 'failure',
                'message' => 'The record is full',
                'suggestion' => 'We see you tomorrow!!',
                'html' => $employeeAssistanceHtml,
            );
//            var_dump($respuesta);
        } else if ($chk === false) {
            //if record is incomplete then the user have not any in/out yet, so is necesary insert it with the check-in
            $employee = $this->checkEmployeeExist($ci);
            $employeeId = $employee['id'];
            $this->addEmployeeRecord($employeeId);
            //response
            $employeeAssistanceHtml = $this->getHtml($ci);
            $respuesta = array(
                'status' => 'success',
                'html' => $employeeAssistanceHtml
            );
//            var_dump($respuesta);
        } else {
            // si no retorna true entonces faltan registros por actualizar
            $fieldToUpdate = $chk;
            $employee = $this->checkEmployeeExist($ci);
            $employeeId = $employee['id'];
            $this->updateEmployeeRecord($employeeId, $fieldToUpdate);

            //response
            $recordSet = $this->checkEmployeeRecord($ci);
            $chk = $this->checkFullEmployeeRecord($recordSet);
            $employeeAssistanceHtml = $this->getHtml($ci);
            if ($chk === true) {
                $respuesta = array(
                    'status' => 'failure',
                    'message' => 'The record has been completed',
                    'suggestion' => 'We see you tomorrow!!',
                    'html' => $employeeAssistanceHtml,
                );
            } else {
                $respuesta = array(
                    'status' => 'success',
                    'html' => $employeeAssistanceHtml
                );
            }
        }
        return $respuesta;
    }

}

?>
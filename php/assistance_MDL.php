<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/_testrey/everytest/php/AssistanceCore.php');

//declare a static array
$ci = $_GET['ci'];

//create instance of assistance core
$assistanceHandler = new AssistanceCore();

//  if employee doesn't exists return a fault json reporting it 
if (!$assistanceHandler->checkEmployeeExist($ci)) {
    $respuesta = array(
        'status' => 'failure',
        'message' => 'The DNI is not registered',
        'suggestion' => 'Please type it again');
} else { //if employee exists get their day registration  
    $respuesta = $assistanceHandler->getResponse($ci);  
}
$respuesta = array_map('utf8_encode', $respuesta);
echo json_encode($respuesta);
die();


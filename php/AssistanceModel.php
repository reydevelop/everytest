<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/Common/DAO.php');

class AssistanceModel {

    // properties of the class
    private $daoHandler = null;

    /*
      Constructor: __construct
      Initialized the NotesModel object and assigns a new DAO object to property $daoHandler.
     */

    public function __construct() {
        $this->daoHandler = new DAO();
    }

    /**
     * Function: CheckEmployee
     * check by ci if employee is registered
     * Parameters: ci number
     * Return: true or false
     */
    public function getEmployee($ci) {
        $sql = "SELECT * FROM `rey_employee` where `ci`= ?";
        $this->daoHandler->resetDao();
        $this->daoHandler->setQuery($sql);
        $this->daoHandler->setParameter('s', $ci);
        $employee = $this->daoHandler->executeQuery();
        return $employee;
    }

    public function getEmployeeDayRecord($ci) {
        $today = date('Y-m-d');
        $sql = "SELECT * FROM rey_employee e INNER JOIN rey_assistance a  ON e.id = a.rey_employee_id WHERE e.ci = ? AND a.date=CAST( ? AS DATE)";
        $this->daoHandler->resetDao();
        $this->daoHandler->setQuery($sql);
        $this->daoHandler->setParameter('s', $ci);
        $this->daoHandler->setParameter('s', $today);
        $result = $this->daoHandler->executeQuery();
        return $result[0];
    }

    public function updateEmployeeDayRecord($employeeId, $fieldToUpdate) {
        $sql = "UPDATE rey_assistance SET ".$fieldToUpdate." = ? WHERE `rey_employee_id` = ? AND `date`=CAST( ? AS DATE)";
        $today = date('Y-m-d');
        $now = date('H:i:s');
        $this->daoHandler->resetDao();
        $this->daoHandler->setQuery($sql);
        $this->daoHandler->setParameter('s', $now);
        $this->daoHandler->setParameter('i', $employeeId);
        $this->daoHandler->setParameter('s', $today);
        $this->daoHandler->executeQuery();
    }
    public function insertEmployeeDayRecord($employeeId) {
        $sql = "INSERT INTO `rey_assistance` (`date`, `chk_in`, `rey_employee_id`) VALUES (? , ? , ?)";
        $today = date('Y-m-d');
        $now = date('H:i:s');
        $this->daoHandler->resetDao();
        $this->daoHandler->setQuery($sql);
        $this->daoHandler->setParameter('s', $today);
        $this->daoHandler->setParameter('s', $now);
        $this->daoHandler->setParameter('i', $employeeId);
        $this->daoHandler->executeQuery();
    }


}

?>

<?php

class AssistanceView {

    public function __construct() {
        
    }

    private function parseTemplate($searchArray, $replaceArray, $html) {
        return str_replace($searchArray, $replaceArray, $html);
    }

    public function AssistanceTemplate() {
        $html = '       <h3>Name: <span id="employee_name"> ###name### </span></h3>
                        <p><strong>Check In: </strong><span id="chk_in"> ###chk_in### </span></p>
                        <p><strong>Lunch Out: </strong><span id="lunch_out"> ###lunch_out### </span></p>
                        <p><strong>Lunch In: </strong><span id="lunch_in"> ###lunch_in### </span></p>
                        <p><strong>Check Out: </strong><span id="chk_out"> ###chk_out### </span></p>
                    ';
        return $html;
    }

    public function AssistanceView($assistanceInfoArray) { // $assistanceInfoArray should be an array that each contain the values
        // set the array that contains the placeholders
        $searchArray = array("###name###", "###chk_in###", "###lunch_out###", "###lunch_in###", "###chk_out###");

        // create an empty string to hold the note html string
        $assistanceHtml = "";
        $html = $this->AssistanceTemplate(); // get the html template that contains palceholders to replace with values from $assistanceInfoArray
            $assistanceHtml .= $this->parseTemplate($searchArray, $assistanceInfoArray, $html) . "\n";
        return $assistanceHtml;
    }

}

?>